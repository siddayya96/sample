const csvToJson = require('csvtojson');
const { Pool } = require('pg');
const pool = new Pool({
    user:'siddz',
    database:'ipl',
    password:'root',
});

csvToJson({ checkType: true }).fromFile('./deliveries.csv').then(
        deliveriesData => {
            var arrayOfPromises=deliveriesData.map(item=>{
            return new Promise((resolve,reject)=>{
                var obj=Object.values(item);
                pool.connect().then((client)=>{
                client.query("insert into deliveries1 values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21) ",
                obj).then(()=>{
                    client.release();
                     resolve("data is inserted");
                })
                .catch((err)=>{
                    client.release(); 
                    reject(err);
                })
            })
            });
        });
        Promise.all(arrayOfPromises).then((result)=>{
            console.log(result)
            pool.end()
        }).catch((err)=>{
            console.log(err);
        })
    }).catch(err=>console.log('file not found',err))




